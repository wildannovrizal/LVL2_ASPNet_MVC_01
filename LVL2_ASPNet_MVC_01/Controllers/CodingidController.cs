﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LVL2_ASPNet_MVC_01.Controllers
{
    public class CodingidController : Controller
    {
        // GET: Codingid
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CodingIDMessage()
        {
            ViewBag.Message = "Learn, Code, Share";
            return View();
        }
    }
}